package ru.xdpi.curiousbunny;

public class ConstantFlavor {
    public static final BuildType BUILD_TYPE = BuildType.PAID;
    public static final String URL_MARKET_CURIOUS_BUNNY = "market://details?id=ru.xdpi.curiousbunny";
    public static final String URL_MARKET_SHARE = "https://market.android.com/details?id=ru.xdpi.curiousbunny";
    public static final String URL_CURIOUS_BUNNY = "https://play.google.com/store/apps/details?id=ru.xdpi.curiousbunny";
    public static final String PROPERTY_ID = "UA-49516762-4";
}
