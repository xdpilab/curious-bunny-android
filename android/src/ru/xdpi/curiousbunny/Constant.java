package ru.xdpi.curiousbunny;

public final class Constant {
    // AdMob
    public static final String BIG_BANNER_SETTINGS_ENTER = "ca-app-pub-3589613034203320/9714435095";
    public static final String BIG_BANNER_SETTINGS_ESC = "ca-app-pub-3589613034203320/4906024290";
    public static final String SMART_BANNER = "ca-app-pub-3589613034203320/4059049890";

    public static final String SMALL_BANNER = "ca-app-pub-3589613034203320/2524323094";


    // AdColony
    public static final String AD_COLONY_APP_ID = "app293899bba53342dda3";
    public static final String AD_COLONY_CLIENT_OPTIONS = "version:1.5,store:google";
    public static final String AD_COLONY_ZONE_CLOSE_APP = "vz1d2e41ef00fe47a9bf";


    // Google Play
    public static final String XDPI_LAB = "market://search?q=pub:XDPI Lab";
    public static final String CURIOUS_BUNNY = "market://details?id=ru.xdpi.curiousbunny";
    public static final String E_MAIL = "xdpilab@gmail.com";


    // Developer Console
    public static final String BASE_64_ENCODED_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmrFU4LynL00n3jCUVJtL7FUWFEvH6MGRbXUGdNXzHjvTQIFUagftMR2fRxzeJJoPY1HHkDUxRettTQtRfDCAl7Pcmdx/FQ33jyYEGGHo7a2dP40/pCRO8q/yseuRikBPnrvIU9uaZrY5z4LwMV/0Bpt+6bhy04b+IQAOKvI2HVUwxZNFA8EZ9GE98FbhfgTTTir7I+Naa5demWXOyCfkIl3rB9eIMUed/4cwo3OO40zCryEh64H4wViJlGczjlTFp2cBuO4i69nxp5BofPBXT9ZFYKSgx3zkZE1mX36rS63QmBvgWpffL7NUNKFdBbfNO0s0zSlIe5hlY6sv1s10KQIDAQAB";

    public static final String SKU_TEST_ITEM = "android.test.purchased";

    public static final String SKU_SPRING_BLUE_TIT = "spring_blue_tit"; // 201
    public static final String SKU_SPRING_CAP = "spring_cap"; // 202
    public static final String SKU_SUMMER_SOMBRERO = "summer_sombrero"; // 301
    public static final String SKU_AUTUMN_HEDGEHOG = "autumn_hedgehog"; // 401
    public static final String SKU_AUTUMN_SCARF = "autumn_scarf"; // 402
    public static final String SKU_AUTUMN_PEAKED_CAP = "autumn_peaked_cap"; // 403

    public static final String SP_SPRING_BLUE_TIT = "blue_tit_save";
    public static final String SP_SPRING_CAP = "spring_cap_save";
    public static final String SP_SUMMER_SOMBRERO = "summer_sombrero_save";
    public static final String SP_AUTUMN_HEDGEHOG = "autumn_hedgehog_save";
    public static final String SP_AUTUMN_SCARF = "autumn_scarf_save";
    public static final String SP_AUTUMN_PEAKED_CAP = "autumn_peaked_cap_save";

}
