package ru.xdpi.curiousbunny.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import ru.xdpi.curiousbunny.Constant;
import ru.xdpi.curiousbunny.LiveWallpaper;
import ru.xdpi.curiousbunny.R;
import ru.xdpi.curiousbunny.googleservice.Analytics;
import ru.xdpi.curiousbunny.util.IabHelper;
import ru.xdpi.curiousbunny.util.IabResult;
import ru.xdpi.curiousbunny.util.Inventory;
import ru.xdpi.curiousbunny.util.Purchase;

public class Settings extends PreferenceFragment implements Preference.OnPreferenceClickListener {
    private static final String TAG = "fragment." + Settings.class.getSimpleName();

    private boolean mHasSpringBlueTit, mHasSpringCap, mHasSummerSombrero, mHasAutumnHedgehog, mHasAutumnScarf, mHasAutumnPeakedCap;

    private SharedPreferences mSp;
    private SharedPreferences.Editor mEd;
    private CheckBoxPreference mChbBlueTit, mChbCap, mChbSombrero, mChbAutumnHedgehog, mChbScarf, mChbAutumnCap;
    private IabHelper mHelper;

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        getPreferenceManager().setSharedPreferencesName(LiveWallpaper.SHARED_PREFS_NAME);
        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mEd = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();

        checkSku();

        mChbBlueTit = (CheckBoxPreference) findPreference(getString(R.string.key_chb_blue_tit));
        mChbCap = (CheckBoxPreference) findPreference(getString(R.string.key_chb_cap));
        mChbSombrero = (CheckBoxPreference) findPreference(getString(R.string.key_chb_sombrero));
        mChbAutumnHedgehog = (CheckBoxPreference) findPreference(getString(R.string.key_chb_autumn_hedgehog));
        mChbScarf = (CheckBoxPreference) findPreference(getString(R.string.key_chb_scarf));
        mChbAutumnCap = (CheckBoxPreference) findPreference(getString(R.string.key_chb_autumn_cap));
        Preference aboutDialog = findPreference(getString(R.string.key_about));

        mHelper = new IabHelper(getActivity(), Constant.BASE_64_ENCODED_PUBLIC_KEY);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
//                    LOG.i(TAG, "mHelper 0 " + result.getMessage());
                    return;
                }
                mHelper.queryInventoryAsync(mGotInventoryListener);
//                LOG.i(TAG, "mHelper 1 " + result.getMessage());
            }
        });

        mChbBlueTit.setOnPreferenceClickListener(this);
        mChbCap.setOnPreferenceClickListener(this);
        mChbSombrero.setOnPreferenceClickListener(this);
        mChbAutumnHedgehog.setOnPreferenceClickListener(this);
        mChbScarf.setOnPreferenceClickListener(this);
        mChbAutumnCap.setOnPreferenceClickListener(this);
        aboutDialog.setOnPreferenceClickListener(this);

        Analytics.setAppTracker(getActivity(), TAG);
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (mHelper == null)
                return;

            if (result.isFailure()) {
//                if (result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED)
//                    serPremium();

//                LOG.i(TAG, "mGotInventoryListener 0 " + result.getMessage());
                return;
            }

            mHasSpringBlueTit = inventory.hasPurchase(Constant.SKU_SPRING_BLUE_TIT);
            mHasSpringCap = inventory.hasPurchase(Constant.SKU_SPRING_CAP);
            mHasSummerSombrero = inventory.hasPurchase(Constant.SKU_SUMMER_SOMBRERO);
            mHasAutumnHedgehog = inventory.hasPurchase(Constant.SKU_AUTUMN_HEDGEHOG);
            mHasAutumnScarf = inventory.hasPurchase(Constant.SKU_AUTUMN_SCARF);
            mHasAutumnPeakedCap = inventory.hasPurchase(Constant.SKU_AUTUMN_PEAKED_CAP);

            mEd.putBoolean(Constant.SP_SPRING_BLUE_TIT, mHasSpringBlueTit);
            mEd.putBoolean(Constant.SP_SPRING_CAP, mHasSpringCap);
            mEd.putBoolean(Constant.SP_SUMMER_SOMBRERO, mHasSummerSombrero);
            mEd.putBoolean(Constant.SP_AUTUMN_HEDGEHOG, mHasAutumnHedgehog);
            mEd.putBoolean(Constant.SP_AUTUMN_SCARF, mHasAutumnScarf);
            mEd.putBoolean(Constant.SP_AUTUMN_PEAKED_CAP, mHasAutumnPeakedCap);
            mEd.apply();
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (mHelper == null)
                return;

            if (result.isFailure()) {
                return;
            }

            if (purchase.getSku().equals(Constant.SKU_SPRING_BLUE_TIT)) {
                mEd.putBoolean(Constant.SP_SPRING_BLUE_TIT, true);
                mEd.apply();
            }

            if (purchase.getSku().equals(Constant.SKU_SPRING_CAP)) {
                mEd.putBoolean(Constant.SP_SPRING_CAP, true);
                mEd.apply();
            }

            if (purchase.getSku().equals(Constant.SKU_SUMMER_SOMBRERO)) {
                mEd.putBoolean(Constant.SP_SUMMER_SOMBRERO, true);
                mEd.apply();
            }

            if (purchase.getSku().equals(Constant.SKU_AUTUMN_HEDGEHOG)) {
                mEd.putBoolean(Constant.SP_AUTUMN_HEDGEHOG, true);
                mEd.apply();
            }

            if (purchase.getSku().equals(Constant.SKU_AUTUMN_SCARF)) {
                mEd.putBoolean(Constant.SP_AUTUMN_SCARF, true);
                mEd.apply();
            }

            if (purchase.getSku().equals(Constant.SKU_AUTUMN_PEAKED_CAP)) {
                mEd.putBoolean(Constant.SP_AUTUMN_PEAKED_CAP, true);
                mEd.apply();
            }
        }
    };

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals(getString(R.string.key_chb_blue_tit))) {
            if (!mHasSpringBlueTit)
                mChbBlueTit.setChecked(false);

                if (mHelper != null) {
                    mHelper.flagEndAsync();
                    String payload = "";
                    mHelper.launchPurchaseFlow(getActivity(), Constant.SKU_SPRING_BLUE_TIT, 201, mPurchaseFinishedListener, payload);
                }


        } else if (preference.getKey().equals(getString(R.string.key_chb_cap))) {
            if (!mHasSpringCap)
                mChbCap.setChecked(false);

                if (mHelper != null) {
                    mHelper.flagEndAsync();
                    String payload = "";
                    mHelper.launchPurchaseFlow(getActivity(), Constant.SKU_SPRING_CAP, 202, mPurchaseFinishedListener, payload);
                }


        } else if (preference.getKey().equals(getString(R.string.key_chb_sombrero))) {
            if (!mHasSummerSombrero)
                mChbSombrero.setChecked(false);

                if (mHelper != null) {
                    mHelper.flagEndAsync();
                    String payload = "";
                    mHelper.launchPurchaseFlow(getActivity(), Constant.SKU_SUMMER_SOMBRERO, 301, mPurchaseFinishedListener, payload);
                }


        } else if (preference.getKey().equals(getString(R.string.key_chb_autumn_hedgehog))) {
            if (!mHasAutumnHedgehog)
                mChbAutumnHedgehog.setChecked(false);

                if (mHelper != null) {
                    mHelper.flagEndAsync();
                    String payload = "";
                    mHelper.launchPurchaseFlow(getActivity(), Constant.SKU_AUTUMN_HEDGEHOG, 401, mPurchaseFinishedListener, payload);
                }


        } else if (preference.getKey().equals(getString(R.string.key_chb_scarf))) {
            if (!mHasAutumnScarf)
                mChbScarf.setChecked(false);

                if (mHelper != null) {
                    mHelper.flagEndAsync();
                    String payload = "";
                    mHelper.launchPurchaseFlow(getActivity(), Constant.SKU_AUTUMN_SCARF, 402, mPurchaseFinishedListener, payload);
                }


        } else if (preference.getKey().equals(getString(R.string.key_chb_autumn_cap))) {
            if (!mHasAutumnPeakedCap)
                mChbAutumnCap.setChecked(false);

                if (mHelper != null) {
                    mHelper.flagEndAsync();
                    String payload = "";
                    mHelper.launchPurchaseFlow(getActivity(), Constant.SKU_AUTUMN_PEAKED_CAP, 403, mPurchaseFinishedListener, payload);
                }


        } else if (preference.getKey().equals(getString(R.string.key_about))) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setTitle(R.string.about_set_title);
            dialog.setMessage(R.string.about_text);
            dialog.setPositiveButton(R.string.about_set_positive_button,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = dialog.create();
            alert.show();
            ((TextView) alert.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
        }

        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // very important
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    private void checkSku() {
        mHasSpringBlueTit = mSp.getBoolean(Constant.SP_SPRING_BLUE_TIT, false);
        mHasSpringCap = mSp.getBoolean(Constant.SP_SPRING_CAP, false);
        mHasSummerSombrero = mSp.getBoolean(Constant.SP_SUMMER_SOMBRERO, false);
        mHasAutumnHedgehog = mSp.getBoolean(Constant.SP_AUTUMN_HEDGEHOG, false);
        mHasAutumnScarf = mSp.getBoolean(Constant.SP_AUTUMN_SCARF, false);
        mHasAutumnPeakedCap = mSp.getBoolean(Constant.SP_AUTUMN_PEAKED_CAP, false);
    }

}