package ru.xdpi.curiousbunny;

import android.app.Application;
import android.text.TextUtils;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();
//    private static final String PROPERTY_ID = ConstantFlavor.PROPERTY_ID;

    /**
     * Global request queue for Volley
     */
//    private RequestQueue mRequestQueue;

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static AppController sInstance;

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     * <p/>
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
        APP_TRACKER, // tracker used only in this app.
        GLOBAL_TRACKER, // tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // tracker used by all ecommerce transactions from a company.
    }

//    HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    public AppController() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // init the singleton
        sInstance = this;


        // init Facebook
//        FacebookSdk.sdkInitialize(this);
    }

    /**
     * @return Contact Lenses singleton instance
     */
    public static synchronized AppController getInstance() {
        return sInstance;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    /**
     * Get tracker for Google Analytics
     *
     * @param trackerId
     */
//    public synchronized Tracker getTracker(TrackerName trackerId) {
//        if (!mTrackers.containsKey(trackerId)) {
//            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
//                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
//                    : analytics.newTracker(R.xml.ecommerce_tracker);
//            mTrackers.put(trackerId, t);
//        }
//        return mTrackers.get(trackerId);
//    }

}