package ru.xdpi.curiousbunny;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.util.DisplayMetrics;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;

import ru.xdpi.curiousbunny.dialog.SeekBarPreference;

public class LiveWallpaper extends AndroidLiveWallpaperService implements OnSharedPreferenceChangeListener {

    public static final String SHARED_PREFS_NAME = "SettingsActivity";

    private SharedPreferences mSharedPreferences;

    public static boolean EYES_MOVEMENT = true;
    public static String EYES_COLOR = "0";
    public static boolean HAT = true;
    public static boolean WAISTCOAT = false;
    public static boolean CAP = true;
    public static boolean GLASSES = true;
    public static boolean SOMBRERO = false;
    public static boolean SCARF = true;
    public static boolean AUTUMN_CAP = false;

    public static boolean BULLFINCH = true;
    public static boolean BLUE_TIT = true;
    public static boolean CATERPILLAR = true;
    public static boolean HEDGEHOG = true;
    public static boolean SUN = true;
    public static boolean SUN_ANIMATION = true;
    public static boolean CLOUDS = true;
    public static String CLOUDS_MOVE = "0";
    public static String CLOUDS_VELOCITY = "0";
    public static boolean RAINBOW = true;

    public static String SEASON = "0";

    public static String SP_CHB_EYE_MOVEMENT = "chb_eye_movement";
    public static String SP_EYES_COLOR = "eyes_color";
    public static String SP_CHB_HAT = "chb_hat";
    public static String SP_CHB_WAISTCOAT = "chb_waistcoat";
    public static String SP_CHB_CAP = "chb_cap";
    public static String SP_CHB_GLASSES = "chb_glasses";
    public static String SP_CHB_SOMBRERO = "chb_sombrero";
    public static String SP_CHB_SCARF = "chb_scarf";
    public static String SP_AUTUMN_CAP = "chb_autumn_cap";
    public static String SP_BULLFINCH = "chb_bullfinch";
    public static String SP_CHB_BLUE_TIT = "chb_blue_tit";
    public static String SP_CATERPILLAR = "chb_caterpillar";
    public static String SP_CHB_AUTUMN_HEDGEHOG = "chb_autumn_hedgehog";
    public static String SP_SUN = "sun";
    public static String SP_SUN_ANIMATION = "sun_animation";
    public static String SP_CLOUDS = "clouds";
    public static String SP_CLOUDS_MOVE = "clouds_move";
    public static String SP_CLOUDS_VELOCITY = "cloud_velocity";
    public static String SP_RAINBOW = "rainbow";
    public static String SP_SEASONS = "seasons";

    public static String SP_SEEK_BAR = "seekBarPreference";

    @Override
    public void onCreate() {
        super.onCreate();
        mSharedPreferences = getSharedPreferences(SHARED_PREFS_NAME, 0);
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);

        EYES_MOVEMENT = mSharedPreferences.getBoolean(SP_CHB_EYE_MOVEMENT, true);
        EYES_COLOR = mSharedPreferences.getString(SP_EYES_COLOR, "0");
        HAT = mSharedPreferences.getBoolean(SP_CHB_HAT, true);
        WAISTCOAT = mSharedPreferences.getBoolean(SP_CHB_WAISTCOAT, false);
        CAP = mSharedPreferences.getBoolean(SP_CHB_CAP, true);
        GLASSES = mSharedPreferences.getBoolean(SP_CHB_GLASSES, true);
        SOMBRERO = mSharedPreferences.getBoolean(SP_CHB_SOMBRERO, false);
        SCARF = mSharedPreferences.getBoolean(SP_CHB_SCARF, true);
        AUTUMN_CAP = mSharedPreferences.getBoolean(SP_AUTUMN_CAP, false);

        BULLFINCH = mSharedPreferences.getBoolean(SP_BULLFINCH, true);
        BLUE_TIT = mSharedPreferences.getBoolean(SP_CHB_BLUE_TIT, true);
        CATERPILLAR = mSharedPreferences.getBoolean(SP_CATERPILLAR, true);
        HEDGEHOG = mSharedPreferences.getBoolean(SP_CHB_AUTUMN_HEDGEHOG, true);
        SUN = mSharedPreferences.getBoolean(SP_SUN, true);
        SUN_ANIMATION = mSharedPreferences.getBoolean(SP_SUN_ANIMATION, true);
        CLOUDS = mSharedPreferences.getBoolean(SP_CLOUDS, true);
        CLOUDS_MOVE = mSharedPreferences.getString(SP_CLOUDS_MOVE, "0");
        CLOUDS_VELOCITY = mSharedPreferences.getString(SP_CLOUDS_VELOCITY, "0");
        RAINBOW = mSharedPreferences.getBoolean(SP_RAINBOW, true);

        SEASON = mSharedPreferences.getString(SP_SEASONS, "0");

        load();
    }

    @Override
    public void onCreateApplication() {
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
//        config.useGL20 = true;

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        switch (metrics.densityDpi) {
            case DisplayMetrics.DENSITY_MEDIUM:
                ApplicationListener listenerMdpi = new LiveWallpaperListenerMDPI();
                initialize(listenerMdpi, config);
                break;

            case DisplayMetrics.DENSITY_HIGH:
                ApplicationListener listenerHdpi = new LiveWallpaperListenerHDPI();
                initialize(listenerHdpi, config);
                break;

            case DisplayMetrics.DENSITY_XHIGH:
                ApplicationListener listenerXhdpi = new LiveWallpaperListenerXHDPI();
                initialize(listenerXhdpi, config);
                break;

            case DisplayMetrics.DENSITY_XXHIGH:
                ApplicationListener listenerXxhdpi = new LiveWallpaperListenerXXHDPI();
                initialize(listenerXxhdpi, config);
                break;
        }
        super.onCreateApplication();
    }

    @Override
    public void initialize(ApplicationListener listener, AndroidApplicationConfiguration config) {
        super.initialize(listener, config);
    }

//    @Override
//    public void initialize(ApplicationListener listener, boolean useGL2IfAvailable) {
//        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
//        config.useGL20 = useGL2IfAvailable;
//        initialize(listener, config);
//        super.initialize(listener, useGL2IfAvailable);
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        save();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        EYES_MOVEMENT = sharedPreferences.getBoolean(SP_CHB_EYE_MOVEMENT, true);
        EYES_COLOR = sharedPreferences.getString(SP_EYES_COLOR, "0");
        HAT = sharedPreferences.getBoolean(SP_CHB_HAT, true);
        WAISTCOAT = sharedPreferences.getBoolean(SP_CHB_WAISTCOAT, false);
        CAP = sharedPreferences.getBoolean(SP_CHB_CAP, true);
        GLASSES = sharedPreferences.getBoolean(SP_CHB_GLASSES, true);
        SOMBRERO = sharedPreferences.getBoolean(SP_CHB_SOMBRERO, false);
        SCARF = sharedPreferences.getBoolean(SP_CHB_SCARF, true);
        AUTUMN_CAP = sharedPreferences.getBoolean(SP_AUTUMN_CAP, false);

        BULLFINCH = sharedPreferences.getBoolean(SP_BULLFINCH, true);
        BLUE_TIT = sharedPreferences.getBoolean(SP_CHB_BLUE_TIT, true);
        CATERPILLAR = sharedPreferences.getBoolean(SP_CATERPILLAR, true);
        SOMBRERO = sharedPreferences.getBoolean(SP_CHB_AUTUMN_HEDGEHOG, true);
        SUN = sharedPreferences.getBoolean(SP_SUN, true);
        SUN_ANIMATION = sharedPreferences.getBoolean(SP_SUN_ANIMATION, true);
        CLOUDS = sharedPreferences.getBoolean(SP_CLOUDS, true);
        CLOUDS_MOVE = sharedPreferences.getString(SP_CLOUDS_MOVE, "0");
        CLOUDS_VELOCITY = sharedPreferences.getString(SP_CLOUDS_VELOCITY, "0");
        RAINBOW = sharedPreferences.getBoolean(SP_RAINBOW, true);

        SEASON = sharedPreferences.getString(SP_SEASONS, "0");
    }

    private void save() {
        Editor editor = mSharedPreferences.edit();
        editor.putInt(SP_SEEK_BAR, SeekBarPreference.mCurrentValue);
        editor.apply();
    }

    private void load() {
        SeekBarPreference.mCurrentValue = mSharedPreferences.getInt(SP_SEEK_BAR, SeekBarPreference.mCurrentValue);
    }

}
