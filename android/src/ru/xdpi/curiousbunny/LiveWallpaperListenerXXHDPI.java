package ru.xdpi.curiousbunny;

import java.util.Calendar;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;

import ru.xdpi.curiousbunny.character.BlueTit;
import ru.xdpi.curiousbunny.character.Bullfinch;
import ru.xdpi.curiousbunny.character.Caterpillar;
import ru.xdpi.curiousbunny.character.Cloud;
import ru.xdpi.curiousbunny.character.Hedgehog;
import ru.xdpi.curiousbunny.dialog.SeekBarPreference;

public class LiveWallpaperListenerXXHDPI implements ApplicationListener {
	private Texture winter;
	private Texture spring;
	private Texture summer;
	private Texture autumn;
	private Texture sun;
	private TextureAtlas atlas;
	private TextureRegion autumnBirch;
	private TextureRegion eyesbottom;
	private TextureRegion bunny;
	private TextureRegion glasses;
	private TextureRegion[][] regions;
	private TextureRegion[] sunRegion;
	private int SCREEN_WIDTH;
	private int SCREEN_HEIGHT;
	private int DELTA_SCREEN_X;
	private int DELTA_SCREEN_Y;
	private int bunnyX;
	private int bunnyY;
	private int glassesX;
	private int glassesY;
	private int eyesRectangX;
	private int eyesRectangY;
	private int eyesbottomX;
	private int eyesbottomY;
	private int birchX;
	private int birchY;
	private int touchpadX;
	private int touchpadY;
	private Animation sunAnimation;
	private TextureRegion cloud1;
	private Cloud[] cl1;
	private TextureRegion cloud2;
	private Cloud[] cl2;
	private Stage stage;
	private TextureRegion eyesRectangle;
	private Image eyesRectangleImage;
	private static final float SUN_ANIMATION_SPEED = 0.13f;
	private static final float BULLFINCH_ANIMATION_SPEED = 0.07f;
	private static final float BLUE_TIT_ANIMATION_SPEED = 0.07f;
	private static final float CATERPILLAR_ANIMATION_SPEED = 0.1f;
	private static final float HEDGEHOB_ANIMATION_SPEED = 0.1f;
	private float stateTime;
	private TextureRegion sunFrame;
	private SpriteBatch batch;
	private SpriteBatch batchStuff;
	private TextureRegion winterBirch;
	private TextureRegion wbird_currentFrame;
	private TextureRegion springBirch;
	private TextureRegion summerBirch;
	private int month;
	private int cl1Y;
	private int cl2Y;
	private int cl11Y;
	private int cl12Y;
	private int cl13Y;
	private TextureRegion cloud3;
	private Cloud[] cl3;
	private TextureRegion cloud4;
	private Cloud[] cl4;
	private int cl3Y;
	private int cl4Y;
	private TextureRegion scarf;
	private int scarfX;
	private int scarfY;
	private TextureRegion hat;
	private int hatX;
	private int hatY;
	private TextureRegion cap;
	private int capX;
	private int capY;
	private int cl5Y;
	private int cl6Y;
	private TextureRegion cloud5;
	private Cloud[] cl5;
	private TextureRegion cloud6;
	private Cloud[] cl6;
	private TextureRegion cloud7;
	private Cloud[] cl7;
	private TextureRegion cloud8;
	private Cloud[] cl8;
	private int cl7Y;
	private int cl8Y;
	private TextureRegion cloud9;
	private Cloud[] cl9;
	private TextureRegion cloud10;
	private Cloud[] cl10;
	private int cl9Y;
	private int cl10Y;
	private int HedgehogY;
	private Texture autumnHedgehog;
	private TextureRegion[] autumnHedgehogSplit;
	private Hedgehog[] autumnhedgehog;
	private TextureRegion ahedgehog_currentFrame;
	private Animation autumnHedgehogAnimation;
	private TouchpadXXHDPI blueEyes;
	private TouchpadXXHDPI greyEyes;
	private TouchpadXXHDPI greenEyes;
	private TouchpadXXHDPI brownEyes;
	private TouchpadXXHDPI green_lightEyes;
	private TouchpadXXHDPI indigoEyes;
	private TouchpadXXHDPI whiteEyes;
	private Texture bullfinch;
	private TextureRegion[] bullfinchSplit;
	private Animation bullfinchAnimation;
	private Bullfinch[] winterBullfinch;
	private TextureRegion wBullfinch_currentFrame;
	private Texture blueTit;
	private TextureRegion[] blueTitSplit;
	private Animation blueTitAnimation;
	private BlueTit[] springBlueTit;
	private Texture caterpillar;
	private TextureRegion[][] caterpillarSplit;
	private TextureRegion[] caterpillarfFrame;
	private Animation caterpillarAnimation;
	private Caterpillar[] summerCaterpillar;
	private int caterpillarY;
	private TextureRegion scaterpillar_currentFrame;
	private int bullfinchY;
	private int blueTitY;
	private TextureRegion rainbow;
	private int rainbowX;
	private int rainbowY;
	private int sunWinterX;
	private int sunWinterY;
	private int sunSpringX;
	private int sunSpringY;
	private int sunSummerX;
	private int sunSummerY;
	private int sunAutumnX;
	private int sunAutumnY;
	private TextureRegion cloud11;
	private Cloud[] cl11;
	private TextureRegion cloud12;
	private Cloud[] cl12;
	private TextureRegion cloud13;
	private Cloud[] cl13;
	private TextureRegion waistcoat;
	private TextureRegion sombrero;
	private TextureRegion autumn_cap;
	private int waistcoatX;
	private int waistcoatY;
	private int sombreroX;
	private int sombreroY;
	private int autumn_capX;
	private int autumn_capY;
	private static final int CATERPILLAR_FRAME_COLS = 6;
	private static final int CATERPILLAR_FRAME_ROWS = 2;

	@Override
	public void create() {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		SCREEN_HEIGHT = Gdx.graphics.getHeight();

		if (SCREEN_WIDTH != 720) {
			DELTA_SCREEN_X = (SCREEN_WIDTH - 720) / 2;
		}

		if (SCREEN_HEIGHT != 1280) {
			DELTA_SCREEN_Y = (SCREEN_HEIGHT - 1280) / 2;
		}

		// Coordinates ==================================================
		bunnyX = 308 + DELTA_SCREEN_X;
		bunnyY = 368 + DELTA_SCREEN_Y;

		hatX = 370 + DELTA_SCREEN_X;
		hatY = 714 + DELTA_SCREEN_Y;

		waistcoatX = 340 + DELTA_SCREEN_X;
		waistcoatY = 419 + DELTA_SCREEN_Y;

		capX = 345 + DELTA_SCREEN_X;
		capY = 705 + DELTA_SCREEN_Y;

		glassesX = 332 + DELTA_SCREEN_X;
		glassesY = 657 + DELTA_SCREEN_Y;

		sombreroX = 290 + DELTA_SCREEN_X;
		sombreroY = 698 + DELTA_SCREEN_Y;

		scarfX = 347 + DELTA_SCREEN_X;
		scarfY = 452 + DELTA_SCREEN_Y;

		autumn_capX = 346 + DELTA_SCREEN_X;
		autumn_capY = 699 + DELTA_SCREEN_Y;

		eyesRectangX = 362 + DELTA_SCREEN_X;
		eyesRectangY = 653 + DELTA_SCREEN_Y;

		eyesbottomX = 365 + DELTA_SCREEN_X;
		eyesbottomY = 658 + DELTA_SCREEN_Y;

		sunWinterX = 210 + DELTA_SCREEN_X;
		sunWinterY = 860 + DELTA_SCREEN_Y;

		sunSpringX = 350 + DELTA_SCREEN_X;
		sunSpringY = 900 + DELTA_SCREEN_Y;

		sunSummerX = 70 + DELTA_SCREEN_X;
		sunSummerY = 940 + DELTA_SCREEN_Y;

		sunAutumnX = 450 + DELTA_SCREEN_X;
		sunAutumnY = 1025 + DELTA_SCREEN_Y;

		rainbowX = 338 + DELTA_SCREEN_X;
		rainbowY = 744 + DELTA_SCREEN_Y;

		birchX = 220 + DELTA_SCREEN_X;
		birchY = 640 + DELTA_SCREEN_Y;

		touchpadX = 77;
		touchpadY = 55;

		bullfinchY = 910 + DELTA_SCREEN_Y;

		blueTitY = 910 + DELTA_SCREEN_Y;

		caterpillarY = 120 + DELTA_SCREEN_Y;

		HedgehogY = 150 + DELTA_SCREEN_Y;

		cl11Y = 1200 + DELTA_SCREEN_Y;
		cl12Y = 1250 + DELTA_SCREEN_Y;
		cl13Y = 1320 + DELTA_SCREEN_Y;

		cl1Y = 1105 + DELTA_SCREEN_Y;
		cl2Y = 1080 + DELTA_SCREEN_Y;

		cl3Y = 1010 + DELTA_SCREEN_Y;
		cl4Y = 995 + DELTA_SCREEN_Y;

		cl5Y = 920 + DELTA_SCREEN_Y;
		cl6Y = 905 + DELTA_SCREEN_Y;

		cl7Y = 880 + DELTA_SCREEN_Y;
		cl8Y = 865 + DELTA_SCREEN_Y;

		cl9Y = 855 + DELTA_SCREEN_Y;
		cl10Y = 850 + DELTA_SCREEN_Y;

		Calendar calendar = Calendar.getInstance();
		month = calendar.get(Calendar.MONTH);

		atlas = new TextureAtlas(Gdx.files.internal("xxhdpi/atlas.atlas"));

		// Winter ==================================================
		winter = new Texture(Gdx.files.internal("xxhdpi/winter/bg.jpg"));
		winterBirch = new TextureRegion(atlas.findRegion("winter_birch"));
		bullfinch = new Texture(Gdx.files.internal("xxhdpi/winter/bullfinch.png"));
		bullfinchSplit = TextureRegion.split(bullfinch, 70, 70)[0];
		bullfinchAnimation = new Animation(BULLFINCH_ANIMATION_SPEED,
				bullfinchSplit);
		winterBullfinch = new Bullfinch[1];
		for (int i = 0; i < 1; i++) {
			winterBullfinch[i] = new Bullfinch((float) Math.random()
					* SCREEN_WIDTH, bullfinchY, false);
		}

		// Spring ==================================================
		spring = new Texture(Gdx.files.internal("xxhdpi/spring/bg.jpg"));
		springBirch = new TextureRegion(atlas.findRegion("spring_birch"));
		blueTit = new Texture(Gdx.files.internal("xxhdpi/spring/blue_tit.png"));
		blueTitSplit = TextureRegion.split(blueTit, 70, 70)[0];
		blueTitAnimation = new Animation(BLUE_TIT_ANIMATION_SPEED, blueTitSplit);
		springBlueTit = new BlueTit[1];
		for (int i = 0; i < 1; i++) {
			springBlueTit[i] = new BlueTit(
					(float) Math.random() * SCREEN_WIDTH, blueTitY, false);
		}

		// Summer ==================================================
		summer = new Texture(Gdx.files.internal("xxhdpi/summer/bg.jpg"));
		rainbow = new TextureRegion(atlas.findRegion("rainbow"));
		summerBirch = new TextureRegion(atlas.findRegion("summer_birch"));
		caterpillar = new Texture(Gdx.files.internal("xxhdpi/summer/caterpillar.png"));
		caterpillarSplit = TextureRegion.split(caterpillar,
				caterpillar.getWidth() / CATERPILLAR_FRAME_COLS,
				caterpillar.getHeight() / CATERPILLAR_FRAME_ROWS);
		caterpillarfFrame = new TextureRegion[CATERPILLAR_FRAME_COLS
				* CATERPILLAR_FRAME_ROWS];
		int index = 0;
		for (int i = 0; i < CATERPILLAR_FRAME_ROWS; i++) {
			for (int j = 0; j < CATERPILLAR_FRAME_COLS; j++) {
				caterpillarfFrame[index++] = caterpillarSplit[i][j];
			}
		}
		caterpillarAnimation = new Animation(CATERPILLAR_ANIMATION_SPEED,
				caterpillarfFrame);
		summerCaterpillar = new Caterpillar[1];
		for (int i = 0; i < 1; i++) {
			summerCaterpillar[i] = new Caterpillar((float) Math.random()
					* SCREEN_WIDTH, caterpillarY, false);
		}

		// Autumn ==================================================
		autumn = new Texture(Gdx.files.internal("xxhdpi/autumn/bg.jpg"));
		autumnBirch = new TextureRegion(atlas.findRegion("autumn_birch"));
		autumnHedgehog = new Texture(Gdx.files.internal("xxhdpi/autumn/hedgehog.png"));
		autumnHedgehogSplit = TextureRegion.split(autumnHedgehog, 80, 80)[0];
		autumnHedgehogAnimation = new Animation(HEDGEHOB_ANIMATION_SPEED,
				autumnHedgehogSplit);
		autumnhedgehog = new Hedgehog[1];
		for (int i = 0; i < 1; i++) {
			autumnhedgehog[i] = new Hedgehog((float) Math.random()
					* SCREEN_WIDTH, HedgehogY, false);
		}

		// Eyesbottom ==================================================
		eyesbottom = new TextureRegion(atlas.findRegion("eyesbottom"));

		// Bunny ==================================================
		bunny = new TextureRegion(atlas.findRegion("bunny"));

		// Hat ==================================================
		hat = new TextureRegion(atlas.findRegion("hat"));

		// Cap ==================================================
		cap = new TextureRegion(atlas.findRegion("cap"));

		// Glasses ==================================================
		glasses = new TextureRegion(atlas.findRegion("glasses"));

		// Scarf ==================================================
		scarf = new TextureRegion(atlas.findRegion("scarf"));

		// Waistcoat ==================================================
		waistcoat = new TextureRegion(atlas.findRegion("waistcoat"));

		// Sombrero ==================================================
		sombrero = new TextureRegion(atlas.findRegion("sombrero"));

		// Autumn cap ==================================================
		autumn_cap = new TextureRegion(atlas.findRegion("autumn_cap"));

		// Sun ==================================================
		sun = new Texture(Gdx.files.internal("xxhdpi/sun.png"));
		regions = TextureRegion.split(sun, 160, 160);
		sunRegion = regions[0];
		sunAnimation = new Animation(SUN_ANIMATION_SPEED, sunRegion);

		// Clouds ==================================================
		cloud11 = new TextureRegion(atlas.findRegion("260cl1"));
		cl11 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl11[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl11Y);
		}
		cloud12 = new TextureRegion(atlas.findRegion("260cl2"));
		cl12 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl12[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl12Y);
		}
		cloud13 = new TextureRegion(atlas.findRegion("260cl3"));
		cl13 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl13[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl13Y);
		}
		// ==================================================
		cloud1 = new TextureRegion(atlas.findRegion("230cl1"));
		cl1 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl1[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl1Y);
		}
		cloud2 = new TextureRegion(atlas.findRegion("230cl2"));
		cl2 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl2[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl2Y);
		}
		// ==================================================
		cloud3 = new TextureRegion(atlas.findRegion("150cl1"));
		cl3 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl3[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl3Y);
		}
		cloud4 = new TextureRegion(atlas.findRegion("150cl2"));
		cl4 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl4[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl4Y);
		}
		// ==================================================
		cloud5 = new TextureRegion(atlas.findRegion("120cl1"));
		cl5 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl5[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl5Y);
		}
		cloud6 = new TextureRegion(atlas.findRegion("120cl2"));
		cl6 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl6[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl6Y);
		}
		// ==================================================
		cloud7 = new TextureRegion(atlas.findRegion("90cl1"));
		cl7 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl7[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl7Y);
		}
		cloud8 = new TextureRegion(atlas.findRegion("90cl2"));
		cl8 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl8[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl8Y);
		}
		// ==================================================
		cloud9 = new TextureRegion(atlas.findRegion("50cl1"));
		cl9 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl9[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl9Y);
		}
		cloud10 = new TextureRegion(atlas.findRegion("50cl2"));
		cl10 = new Cloud[1];
		for (int i = 0; i < 1; i++) {
			cl10[i] = new Cloud((float) Math.random() * SCREEN_WIDTH, cl10Y);
		}

		// Eyes blue stage ==================================================
		eyesRectangle = new TextureRegion(atlas.findRegion("eyesrectangle"));
		eyesRectangleImage = new Image(eyesRectangle);
		eyesRectangleImage.setPosition(eyesRectangX, eyesRectangY);

		FitViewport view = new FitViewport(SCREEN_WIDTH, SCREEN_HEIGHT);
		stage = new Stage(view);

		Skin greyskin = new Skin(Gdx.files.internal("xxhdpi/eyeskin/grey.json"));
		greyEyes = new TouchpadXXHDPI(20, greyskin);
		greyEyes.setBounds(touchpadX, touchpadY, SCREEN_WIDTH, SCREEN_HEIGHT);

		Skin blueskin = new Skin(Gdx.files.internal("xxhdpi/eyeskin/blue.json"));
		blueEyes = new TouchpadXXHDPI(20, blueskin);
		blueEyes.setBounds(touchpadX, touchpadY, SCREEN_WIDTH, SCREEN_HEIGHT);

		Skin brownskin = new Skin(Gdx.files.internal("xxhdpi/eyeskin/brown.json"));
		brownEyes = new TouchpadXXHDPI(20, brownskin);
		brownEyes.setBounds(touchpadX, touchpadY, SCREEN_WIDTH, SCREEN_HEIGHT);

		Skin greenskin = new Skin(Gdx.files.internal("xxhdpi/eyeskin/green.json"));
		greenEyes = new TouchpadXXHDPI(20, greenskin);
		greenEyes.setBounds(touchpadX, touchpadY, SCREEN_WIDTH, SCREEN_HEIGHT);

		Skin green_lightskin = new Skin(Gdx.files.internal("xxhdpi/eyeskin/green_light.json"));
		green_lightEyes = new TouchpadXXHDPI(20, green_lightskin);
		green_lightEyes.setBounds(touchpadX, touchpadY, SCREEN_WIDTH, SCREEN_HEIGHT);

		Skin indigoskin = new Skin(Gdx.files.internal("xxhdpi/eyeskin/indigo.json"));
		indigoEyes = new TouchpadXXHDPI(20, indigoskin);
		indigoEyes.setBounds(touchpadX, touchpadY, SCREEN_WIDTH, SCREEN_HEIGHT);

		Skin whiteskin = new Skin(Gdx.files.internal("xxhdpi/eyeskin/white.json"));
		whiteEyes = new TouchpadXXHDPI(20, whiteskin);
		whiteEyes.setBounds(touchpadX, touchpadY, SCREEN_WIDTH, SCREEN_HEIGHT);

//		Texture.setEnforcePotImages(false);

		batch = new SpriteBatch();
		batchStuff = new SpriteBatch();
	}

	@Override
	public void dispose() {
		batch.dispose();
		batchStuff.dispose();
		stage.dispose();

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void render() {
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		if (LiveWallpaper.SEASON.matches("0")) {
			switch (month) {
			case Calendar.DECEMBER:
			case Calendar.JANUARY:
			case Calendar.FEBRUARY:
				winterRender();
				break;

			case Calendar.MARCH:
			case Calendar.APRIL:
			case Calendar.MAY:
				springRender();
				break;

			case Calendar.JUNE:
			case Calendar.JULY:
			case Calendar.AUGUST:
				summerRender();
				break;

			case Calendar.SEPTEMBER:
			case Calendar.OCTOBER:
			case Calendar.NOVEMBER:
				autumnRender();
				break;
			}
		} else if (LiveWallpaper.SEASON.matches("1")) {
			winterRender();
		} else if (LiveWallpaper.SEASON.matches("2")) {
			springRender();
		} else if (LiveWallpaper.SEASON.matches("3")) {
			summerRender();
		} else if (LiveWallpaper.SEASON.matches("4")) {
			autumnRender();
		}
	}

	private void winterRender() {
		sunAnimation();
		batch.begin();
		batch.draw(winter, -(winter.getWidth() - SCREEN_WIDTH) / 2,
				-(winter.getHeight() - SCREEN_HEIGHT) / 2);
		if (LiveWallpaper.SUN == true) {
			batch.draw(sunFrame, sunWinterX, sunWinterY);
		}
		if (LiveWallpaper.CLOUDS == true) {
			cloudsRender();
		}
		batch.draw(winterBirch, birchX, birchY);

		if (LiveWallpaper.BULLFINCH == true) {
			for (int i = 0; i < winterBullfinch.length; i++) {
				Bullfinch wBullfinch = winterBullfinch[i];
				wBullfinch_currentFrame = bullfinchAnimation.getKeyFrame(
						wBullfinch.stateTime, true);
				batch.draw(wBullfinch_currentFrame, wBullfinch.pos.x,
						wBullfinch.pos.y);
			}
		}

		batch.draw(eyesbottom, eyesbottomX, eyesbottomY);
		batch.draw(bunny, bunnyX, bunnyY);
		batch.end();

		clouds();

		eyes();

		for (int i = 0; i < winterBullfinch.length; i++) {
			winterBullfinch[i].update(Gdx.graphics.getDeltaTime());
		}

		batchStuff.begin();
		if (LiveWallpaper.HAT == true) {
			batchStuff.draw(hat, hatX, hatY);
		}
		if (LiveWallpaper.WAISTCOAT == true) {
			batchStuff.draw(waistcoat, waistcoatX, waistcoatY);
		}
		batchStuff.end();
	}

	private void springRender() {
		sunAnimation();
		batch.begin();

		batch.draw(spring, -(spring.getWidth() - SCREEN_WIDTH) / 2,
				-(spring.getHeight() - SCREEN_HEIGHT) / 2);
		if (LiveWallpaper.SUN == true) {
			batch.draw(sunFrame, sunSpringX, sunSpringY);
		}
		if (LiveWallpaper.CLOUDS == true) {
			cloudsRender();
		}
		batch.draw(springBirch, birchX, birchY);

		if (LiveWallpaper.BLUE_TIT == true) {
			for (int i = 0; i < springBlueTit.length; i++) {
				BlueTit sBlueTit = springBlueTit[i];
				wbird_currentFrame = blueTitAnimation.getKeyFrame(
						sBlueTit.stateTime, true);
				batch.draw(wbird_currentFrame, sBlueTit.pos.x, sBlueTit.pos.y);
			}
		}
		batch.draw(eyesbottom, eyesbottomX, eyesbottomY);

		batch.draw(bunny, bunnyX, bunnyY);
		batch.end();

		clouds();

		eyes();

		for (int i = 0; i < springBlueTit.length; i++) {
			springBlueTit[i].update(Gdx.graphics.getDeltaTime());
		}

		batchStuff.begin();
		if (LiveWallpaper.CAP == true) {
			batchStuff.draw(cap, capX, capY);
		}
		batchStuff.end();
	}

	private void summerRender() {
		sunAnimation();
		batch.begin();
		batch.draw(summer, -(summer.getWidth() - SCREEN_WIDTH) / 2,
				-(summer.getHeight() - SCREEN_HEIGHT) / 2);
		if (LiveWallpaper.SUN == true) {
			batch.draw(sunFrame, sunSummerX, sunSummerY);
		}
		if (LiveWallpaper.RAINBOW == true) {
			batch.draw(rainbow, rainbowX, rainbowY);
		}
		if (LiveWallpaper.CLOUDS == true) {
			cloudsRender();
		}
		batch.draw(summerBirch, birchX, birchY);
		batch.draw(eyesbottom, eyesbottomX, eyesbottomY);
		batch.draw(bunny, bunnyX, bunnyY);

		if (LiveWallpaper.CATERPILLAR == true) {
			for (int i = 0; i < summerCaterpillar.length; i++) {
				Caterpillar sCaterpillar = summerCaterpillar[i];
				scaterpillar_currentFrame = caterpillarAnimation.getKeyFrame(
						sCaterpillar.stateTime, true);
				batch.draw(scaterpillar_currentFrame, sCaterpillar.pos.x,
						sCaterpillar.pos.y);
			}
		}
		batch.end();

		clouds();

		eyes();

		for (int i = 0; i < summerCaterpillar.length; i++) {
			summerCaterpillar[i].update(Gdx.graphics.getDeltaTime());
		}

		batchStuff.begin();
		if (LiveWallpaper.GLASSES == true) {
			batchStuff.draw(glasses, glassesX, glassesY);
		}
		if (LiveWallpaper.SOMBRERO == true) {
			batchStuff.draw(sombrero, sombreroX, sombreroY);
		}
		batchStuff.end();
	}

	private void autumnRender() {
		sunAnimation();
		batch.begin();
		batch.draw(autumn, -(autumn.getWidth() - SCREEN_WIDTH) / 2,
				-(autumn.getHeight() - SCREEN_HEIGHT) / 2);
		if (LiveWallpaper.SUN == true) {
			batch.draw(sunFrame, sunAutumnX, sunAutumnY);
		}
		if (LiveWallpaper.CLOUDS == true) {
			cloudsRender();
		}

		batch.draw(autumnBirch, birchX, birchY);
		if (LiveWallpaper.HEDGEHOG == true) {
			for (int i = 0; i < autumnhedgehog.length; i++) {
				Hedgehog ahedgehog = autumnhedgehog[i];
				ahedgehog_currentFrame = autumnHedgehogAnimation.getKeyFrame(
						ahedgehog.stateTime, true);
				batch.draw(ahedgehog_currentFrame, ahedgehog.pos.x,
						ahedgehog.pos.y);
			}
		}

		batch.draw(eyesbottom, eyesbottomX, eyesbottomY);
		batch.draw(bunny, bunnyX, bunnyY);
		batch.end();

		clouds();

		eyes();

		for (int i = 0; i < autumnhedgehog.length; i++) {
			autumnhedgehog[i].update(Gdx.graphics.getDeltaTime());
		}

		batchStuff.begin();
		if (LiveWallpaper.SCARF == true) {
			batchStuff.draw(scarf, scarfX, scarfY);
		}
		if (LiveWallpaper.AUTUMN_CAP == true) {
			batchStuff.draw(autumn_cap, autumn_capX, autumn_capY);
		}
		batchStuff.end();
	}

	private void cloudsRender() {
		Cloud.VELOCITY0 = 21 + SeekBarPreference.mCurrentValue;
		Cloud.VELOCITY = 13 + SeekBarPreference.mCurrentValue;
		Cloud.VELOCITY1 = 9 + SeekBarPreference.mCurrentValue;
		Cloud.VELOCITY2 = 5 + SeekBarPreference.mCurrentValue;
		Cloud.VELOCITY3 = 2 + SeekBarPreference.mCurrentValue;
		Cloud.VELOCITY4 = 0.5f + SeekBarPreference.mCurrentValue;
		cloudRenderVelocity();
	}

	private void cloudRenderVelocity() {
		for (int i = 0; i < cl11.length; i++) {
			Cloud cloud_11 = cl11[i];
			batch.draw(cloud11, cloud_11.pos5.x, cloud_11.pos5.y);
		}
		for (int i = 0; i < cl12.length; i++) {
			Cloud cloud_12 = cl12[i];
			batch.draw(cloud12, cloud_12.pos5.x, cloud_12.pos5.y);
		}
		for (int i = 0; i < cl13.length; i++) {
			Cloud cloud_13 = cl13[i];
			batch.draw(cloud13, cloud_13.pos5.x, cloud_13.pos5.y);
		}
		for (int i = 0; i < cl10.length; i++) {
			Cloud cloud_10 = cl10[i];
			batch.draw(cloud10, cloud_10.pos4.x, cloud_10.pos4.y);
		}
		for (int i = 0; i < cl9.length; i++) {
			Cloud cloud_9 = cl9[i];
			batch.draw(cloud9, cloud_9.pos4.x, cloud_9.pos4.y);
		}
		for (int i = 0; i < cl8.length; i++) {
			Cloud cloud_8 = cl8[i];
			batch.draw(cloud8, cloud_8.pos3.x, cloud_8.pos3.y);
		}
		for (int i = 0; i < cl7.length; i++) {
			Cloud cloud_7 = cl7[i];
			batch.draw(cloud7, cloud_7.pos3.x, cloud_7.pos3.y);
		}
		for (int i = 0; i < cl6.length; i++) {
			Cloud cloud_6 = cl6[i];
			batch.draw(cloud6, cloud_6.pos2.x, cloud_6.pos2.y);
		}
		for (int i = 0; i < cl5.length; i++) {
			Cloud cloud_5 = cl5[i];
			batch.draw(cloud5, cloud_5.pos2.x, cloud_5.pos2.y);
		}
		for (int i = 0; i < cl4.length; i++) {
			Cloud cloud_4 = cl4[i];
			batch.draw(cloud4, cloud_4.pos1.x, cloud_4.pos1.y);
		}
		for (int i = 0; i < cl3.length; i++) {
			Cloud cloud_3 = cl3[i];
			batch.draw(cloud3, cloud_3.pos1.x, cloud_3.pos1.y);
		}
		for (int i = 0; i < cl2.length; i++) {
			Cloud cloud_2 = cl2[i];
			batch.draw(cloud2, cloud_2.pos.x, cloud_2.pos.y);
		}
		for (int i = 0; i < cl1.length; i++) {
			Cloud cloud_1 = cl1[i];
			batch.draw(cloud1, cloud_1.pos.x, cloud_1.pos.y);
		}
	}

	private void cloudsMoveLeft() {
		for (int i = 0; i < cl11.length; i++) {
			cl11[i].update10(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl12.length; i++) {
			cl12[i].update10(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl13.length; i++) {
			cl13[i].update10(Gdx.graphics.getDeltaTime());
		}

		for (int i = 0; i < cl10.length; i++) {
			cl10[i].update4(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl9.length; i++) {
			cl9[i].update4(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl8.length; i++) {
			cl8[i].update3(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl7.length; i++) {
			cl7[i].update3(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl6.length; i++) {
			cl6[i].update2(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl5.length; i++) {
			cl5[i].update2(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl4.length; i++) {
			cl4[i].update1(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl3.length; i++) {
			cl3[i].update1(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl2.length; i++) {
			cl2[i].update(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl1.length; i++) {
			cl1[i].update(Gdx.graphics.getDeltaTime());
		}
	}

	private void cloudsMoveRight() {
		for (int i = 0; i < cl11.length; i++) {
			cl11[i].update11(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl12.length; i++) {
			cl12[i].update11(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl13.length; i++) {
			cl13[i].update11(Gdx.graphics.getDeltaTime());
		}

		for (int i = 0; i < cl10.length; i++) {
			cl10[i].update9(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl9.length; i++) {
			cl9[i].update9(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl8.length; i++) {
			cl8[i].update8(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl7.length; i++) {
			cl7[i].update8(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl6.length; i++) {
			cl6[i].update7(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl5.length; i++) {
			cl5[i].update7(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl4.length; i++) {
			cl4[i].update6(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl3.length; i++) {
			cl3[i].update6(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl2.length; i++) {
			cl2[i].update5(Gdx.graphics.getDeltaTime());
		}
		for (int i = 0; i < cl1.length; i++) {
			cl1[i].update5(Gdx.graphics.getDeltaTime());
		}
	}

	private void clouds() {
		if (LiveWallpaper.CLOUDS_MOVE.matches("0")) {
			cloudsMoveLeft();
		} else if (LiveWallpaper.CLOUDS_MOVE.matches("1")) {
			cloudsMoveRight();
		}
	}

	private void changeEyes() {
		if (LiveWallpaper.EYES_COLOR.matches("0")) {
			stage.addActor(greyEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("1")) {
			stage.addActor(blueEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("2")) {
			stage.addActor(brownEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("3")) {
			stage.addActor(greenEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("4")) {
			stage.addActor(green_lightEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("5")) {
			stage.addActor(indigoEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("6")) {
			stage.addActor(whiteEyes);
		}

	}

	private void changeEyesNotMove() {
		if (LiveWallpaper.EYES_COLOR.matches("0")) {
			stage.addActor(greyEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("1")) {
			stage.addActor(blueEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("2")) {
			stage.addActor(brownEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("3")) {
			stage.addActor(greenEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("4")) {
			stage.addActor(green_lightEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("5")) {
			stage.addActor(indigoEyes);
		}

		else if (LiveWallpaper.EYES_COLOR.matches("6")) {
			stage.addActor(whiteEyes);
		}
	}

	private void actSatge() {
		stage.addActor(eyesRectangleImage);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	private void clearSatge() {
		stage.getActors().clear();
	}

	private void eyes() {
		if (LiveWallpaper.EYES_MOVEMENT == true) {
			Gdx.input.setInputProcessor(stage);
			clearSatge();
			changeEyes();
			actSatge();
		} else {
			Gdx.input.setInputProcessor(null);
			clearSatge();
			changeEyesNotMove();
			actSatge();
		}
	}

	private void sunAnimation() {
		if (LiveWallpaper.SUN_ANIMATION == true) {
			stateTime += Gdx.graphics.getDeltaTime();
			sunFrame = sunAnimation.getKeyFrame(stateTime, true);
		} else {
			sunFrame = sunAnimation.getKeyFrame(0);
		}
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}
}
