package ru.xdpi.curiousbunny.character;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class BlueTit {
    static final float VELOCITY = 200;
    public final Vector2 pos;
    public final boolean headsLeft;
    public float stateTime;

    public BlueTit(float x, float y, boolean headsLeft) {
        pos = new Vector2().set(x, y);
        this.headsLeft = headsLeft;
        this.stateTime = (float) Math.random();
    }

    // width of blue tit = 53
    public void update(float deltaTime) {
        int SCREEN_WIDTH = Gdx.graphics.getWidth();
        stateTime += deltaTime;
        pos.x = pos.x + (-VELOCITY * deltaTime);

        double random = (1 + Math.random() * SCREEN_WIDTH * 6);

        if (pos.x < -53)
            pos.x = (float) (SCREEN_WIDTH + random);

    }

}
