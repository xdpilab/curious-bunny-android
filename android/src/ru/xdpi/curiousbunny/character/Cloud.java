package ru.xdpi.curiousbunny.character;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Cloud {
	public static float VELOCITY0 = 21;
	public static float VELOCITY = 17;
	public static float VELOCITY1 = 13;
	public static float VELOCITY2 = 9;
	public static float VELOCITY3 = 5;
	public static float VELOCITY4 = 4;
	public final Vector2 pos;
	public final Vector2 pos1;
	public final Vector2 pos2;
	public final Vector2 pos3;
	public final Vector2 pos4;
	public final Vector2 pos5;
	public float stateTime;
	private int SCREEN_WIDTH;
	private double randomA;
	private double randomB;
	private double randomC;
	private double randomD;
	private double randomE;
	private double randomF;

	public Cloud(float x, float y) {
		pos = new Vector2().set(x, y);
		pos1 = new Vector2().set(x, y);
		pos2 = new Vector2().set(x, y);
		pos3 = new Vector2().set(x, y);
		pos4 = new Vector2().set(x, y);
		pos5 = new Vector2().set(x, y);
		this.stateTime = (float) Math.random();
	}

	// For cloud 260
	public void update10(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos5.x = pos5.x + (-VELOCITY0 * deltaTime);

		randomF = (1 + Math.random() * SCREEN_WIDTH);

		if (pos5.x < -260)
			pos5.x = (float) (SCREEN_WIDTH + randomF);
	}

	// For cloud 230
	public void update(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos.x = pos.x + (-VELOCITY * deltaTime);

		randomA = (1 + Math.random() * SCREEN_WIDTH);

		if (pos.x < -230)
			pos.x = (float) (SCREEN_WIDTH + randomA);
	}

	// For cloud 150
	public void update1(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos1.x = pos1.x + (-VELOCITY1 * deltaTime);

		randomB = (1 + Math.random() * SCREEN_WIDTH);

		if (pos1.x < -150)
			pos1.x = (float) (SCREEN_WIDTH + randomB);
	}

	// For cloud 120
	public void update2(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos2.x = pos2.x + (-VELOCITY2 * deltaTime);

		randomC = (1 + Math.random() * SCREEN_WIDTH);

		if (pos2.x < -120)
			pos2.x = (float) (SCREEN_WIDTH + randomC);
	}

	// For cloud 90
	public void update3(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos3.x = pos3.x + (-VELOCITY3 * deltaTime);

		randomD = (1 + Math.random() * SCREEN_WIDTH);

		if (pos3.x < -90)
			pos3.x = (float) (SCREEN_WIDTH + randomD);
	}

	// For cloud 50
	public void update4(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos4.x = pos4.x + (-VELOCITY4 * deltaTime);

		randomE = (1 + Math.random() * SCREEN_WIDTH);

		if (pos4.x < -50)
			pos4.x = (float) (SCREEN_WIDTH + randomE);
	}

	// ==================================================

	// For cloud 260
	public void update11(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos5.x = pos5.x + (VELOCITY0 * deltaTime);

		randomF = (1 + Math.random() * SCREEN_WIDTH);

		if (pos5.x > SCREEN_WIDTH + 260)
			pos5.x = (float) (-260 - randomF);

	}

	// For cloud 230
	public void update5(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos.x = pos.x + (VELOCITY * deltaTime);

		randomA = (1 + Math.random() * SCREEN_WIDTH);

		if (pos.x > SCREEN_WIDTH + 230)
			pos.x = (float) (-230 - randomA);
	}

	// For cloud 150
	public void update6(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos1.x = pos1.x + (VELOCITY1 * deltaTime);

		randomB = (1 + Math.random() * SCREEN_WIDTH);

		if (pos1.x > SCREEN_WIDTH + 150)
			pos1.x = (float) (-150 - randomB);
	}

	// For cloud 120
	public void update7(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos2.x = pos2.x + (VELOCITY2 * deltaTime);

		randomC = (1 + Math.random() * SCREEN_WIDTH);

		if (pos2.x > SCREEN_WIDTH + 120)
			pos2.x = (float) (-120 - randomC);
	}

	// For cloud 90
	public void update8(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos3.x = pos3.x + (VELOCITY3 * deltaTime);

		randomD = (1 + Math.random() * SCREEN_WIDTH);

		if (pos3.x > SCREEN_WIDTH + 90)
			pos3.x = (float) (-90 - randomD);
	}

	// For cloud 50
	public void update9(float deltaTime) {
		SCREEN_WIDTH = Gdx.graphics.getWidth();
		stateTime += deltaTime;
		pos4.x = pos4.x + (VELOCITY4 * deltaTime);

		randomE = (1 + Math.random() * SCREEN_WIDTH);

		if (pos4.x > SCREEN_WIDTH + 50)
			pos4.x = (float) (-50 - randomE);
	}

}
