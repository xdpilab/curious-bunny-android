package ru.xdpi.curiousbunny.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import ru.xdpi.curiousbunny.BuildConfig;
//import ru.xdpi.curiousbunny.BuildType;
import ru.xdpi.curiousbunny.Constant;
import ru.xdpi.curiousbunny.R;
//import ru.xdpi.curiousbunny.ConstantFlavor;
//import ru.xdpi.curiousbunny.R;
//import ru.xdpi.curiousbunny.dialog.RateApp;
//import ru.xdpi.curiousbunny.fragment.Settings;
//import ru.xdpi.curiousbunny.googleservice.AdMob;

public class SettingsActivity extends AppCompatActivity {
    private static final int LAYOUT = R.layout.activity_settings;
//    private static final int CONTENT = R.id.activity_settings_content_frame;
    private boolean mHasSpringBlueTit, mHasSpringCap, mHasSummerSombrero, mHasAutumnHedgehog, mHasAutumnScarf, mHasAutumnPeakedCap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        openSettings();

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        mHasSpringBlueTit = sp.getBoolean(Constant.SP_SPRING_BLUE_TIT, false);
        mHasSpringCap = sp.getBoolean(Constant.SP_SPRING_CAP, false);
        mHasSummerSombrero = sp.getBoolean(Constant.SP_SUMMER_SOMBRERO, false);
        mHasAutumnHedgehog = sp.getBoolean(Constant.SP_AUTUMN_HEDGEHOG, false);
        mHasAutumnScarf = sp.getBoolean(Constant.SP_AUTUMN_SCARF, false);
        mHasAutumnPeakedCap = sp.getBoolean(Constant.SP_AUTUMN_PEAKED_CAP, false);

//        if (!mHasSpringBlueTit && !mHasSpringCap && !mHasSummerSombrero && !mHasAutumnHedgehog && !mHasAutumnScarf && !mHasAutumnPeakedCap)
            // show big banner
//            if (ConstantFlavor.BUILD_TYPE == BuildType.FREE && !BuildConfig.DEBUG)
//                AdMob.showBigBanner(this, Constant.BIG_BANNER_SETTINGS_ENTER);

        // init AdColony
//        AdColony.configure(this, Constant.AD_COLONY_CLIENT_OPTIONS, Constant.AD_COLONY_APP_ID, Constant.AD_COLONY_ZONE_CLOSE_APP);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        AdColony.resume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        AdColony.pause();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        RateApp.onStart(this);
//        RateApp.showRateDialogIfNeeded(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (!mHasSpringBlueTit && !mHasSpringCap && !mHasSummerSombrero && !mHasAutumnHedgehog && !mHasAutumnScarf && !mHasAutumnPeakedCap)
            // show big banner
//            if (ConstantFlavor.BUILD_TYPE == BuildType.FREE && !BuildConfig.DEBUG) {
//                // show big banner AdMob
////                AdMob.showBigBanner(this, Constant.BIG_BANNER_SETTINGS_ESC);
//
//                // show big banner AdColony
//                AdMob.showBigBannerWhenCloseApp(this, Constant.BIG_BANNER_SETTINGS_ESC);
//            }
    }

    private void openSettings() {
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        Settings settings = new Settings();
//        transaction.add(CONTENT, settings);
//        transaction.commit();
    }

}