package ru.xdpi.curiousbunny.googleservice;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import ru.xdpi.curiousbunny.BuildConfig;
import ru.xdpi.curiousbunny.BuildType;
import ru.xdpi.curiousbunny.Constant;
//import ru.xdpi.curiousbunny.ConstantFlavor;

public class AdMob {
    public static void showSmartBannerTop(String unitId, AdView adView, ViewGroup viewGroup) {
//        if (ConstantFlavor.BUILD_TYPE == BuildType.FREE && !BuildConfig.DEBUG) {
//            adView.setAdUnitId(unitId);
//            adView.setAdSize(AdSize.SMART_BANNER);
//            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
//            viewGroup.addView(adView, layoutParams);
//            AdRequest adRequest = new AdRequest.Builder()
//                    .build();
//            adView.loadAd(adRequest);
//        }
    }

    public static void showSmartBannerBottom(String unitId, AdView adView, ViewGroup viewGroup) {
//        if (ConstantFlavor.BUILD_TYPE == BuildType.FREE && !BuildConfig.DEBUG) {
//            adView.setAdUnitId(unitId);
//            adView.setAdSize(AdSize.SMART_BANNER);
//            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
//            viewGroup.addView(adView, layoutParams);
//            AdRequest adRequest = new AdRequest.Builder()
//                    .build();
//            adView.loadAd(adRequest);
//        }
    }

    public static void showBigBanner(Context c, String unitId) {
        final InterstitialAd banner = new InterstitialAd(c);
        banner.setAdUnitId(unitId);
        AdRequest ar = new AdRequest.Builder().build();
        banner.loadAd(ar);
        banner.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                banner.show();
            }
        });
    }

    public static void showBigBannerWhenCloseApp(Activity c, String unitId) {
        final InterstitialAd banner = new InterstitialAd(c);
        banner.setAdUnitId(unitId);
        AdRequest ar = new AdRequest.Builder().build();
        banner.loadAd(ar);
        banner.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();

                // show big banner AdMob
                banner.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);

                // show big banner AdColony
//                AdColonyVideoAd ad = new AdColonyVideoAd(Constant.AD_COLONY_ZONE_CLOSE_APP);
//                ad.show();
            }
        });
    }

}
