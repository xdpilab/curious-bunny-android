package ru.xdpi.curiousbunny;

import ru.xdpi.curiousbunny.BuildType;

public class ConstantFlavor {
    public static final BuildType BUILD_TYPE = BuildType.FREE;
    public static final String URL_MARKET_CURIOUS_BUNNY = "market://details?id=ru.xdpi.curiousbunnyfree";
    public static final String URL_MARKET_SHARE = "https://market.android.com/details?id=ru.xdpi.curiousbunnyfree";
    public static final String URL_CURIOUS_BUNNY = "https://play.google.com/store/apps/details?id=ru.xdpi.curiousbunnyfree";
    public static final String PROPERTY_ID = "UA-49516762-3";




}
